import { format, parse, isDate, toDate } from 'date-fns';

export const formatDate = (dateStr, formatStr = 'yyyy-MM-dd', parseFormat = 'yyyy-MM-dd') => {
  let date = dateStr;
  if (typeof dateStr === 'string') {
    date = parse(dateStr, parseFormat, new Date());
  } else {
    date = toDate(dateStr);
    if (!isDate(date)) {
      throw new Error(`${dateStr}: must be type of String or Date`);
    }
  }

  return format(date, formatStr);
};
