const defaultTheme = require('tailwindcss/defaultTheme');
const colors = require('tailwindcss/colors');
module.exports = {
  prefix: 'tw-',
  important: true,

  theme: {
    extend: {
      screens: {
        '2xl': '1536px',
        '3xl': '1920px',
      },

      spacing: {
        0.5: '0.125rem',
        1.5: '0.375rem',
        2.5: '0.625rem',
        3.5: '0.875rem',
        9: '2.25rem',
        72: '18rem',
        80: '20rem',
        84: '21rem',
        88: '22rem',
        96: '24rem',
      },

      colors: {
        cyan: colors.cyan,
        teal: colors.teal,
        orange: colors.orange,

        inherit: 'inherit',

        primary: 'var(--color-primary)',
        secondary: 'var(--color-secondary)',
        tertiary: 'var(--color-tertiary)',
        light: 'var(--color-light)',
        medium: 'var(--color-medium)',
        dark: 'var(--color-dark)',
      },

      minWidth: (theme) => theme('spacing'),

      maxWidth: (theme) => ({
        ...theme('spacing'),
        '8xl': '88rem',
        '9xl': '96rem',
        '10xl': '104rem',
      }),

      minHeight: (theme) => theme('spacing'),

      maxHeight: (theme) => ({
        ...theme('spacing'),
        '8xl': '88rem',
        '9xl': '96rem',
        '10xl': '104rem',
      }),

      backgroundColor: (theme) => ({
        main: theme('colors.gray.200'),
      }),

      borderWidth: {
        thin: 'thin',
      },

      fontFamily: {
        sans: ['Microsoft YaHei', ...defaultTheme.fontFamily.sans],
      },
    },

    plugins: [require('@tailwindcss/aspect-ratio'), require('@tailwindcss/line-clamp')],
  },
};
