### Development

```bash
npm run dev
```

### Build for test

```bash
npm run build
```

### Build for production

```bash
npm run build:prod
```
