import Vue from 'vue';
import ECharts from 'vue-echarts';

import 'echarts/lib/component/dataset';
import 'echarts/lib/component/tooltip';
import 'echarts/lib/chart/bar';
import 'echarts/lib/chart/line';
import 'echarts/lib/component/legend';
import 'echarts/lib/component/title';

const availableThemeNameList = ['primary', 'secondary'];

export default async () => {
  Vue.component('ECharts', ECharts);

  const themeList = await Promise.all(
    availableThemeNameList.map((theme) =>
      import(`./theme/${theme}.js`).then((module) => module.default)
    )
  );

  themeList.forEach((theme, index) => {
    const themeId = availableThemeNameList[index];
    ECharts.registerTheme(themeId, theme);
  });

  ECharts.registerMap('china', await import('echarts/map/json/china.json'));
};
