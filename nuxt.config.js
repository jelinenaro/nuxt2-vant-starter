import { join } from 'path';

const APP_NAME = '视频业务质量保障系统';

export default {
  // Disable server-side rendering (https://go.nuxtjs.dev/ssr-mode)
  ssr: false,

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: APP_NAME,
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, viewport-fit=cover',
      },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: `${process.env.ROUTER_ROOT}/favicon.ico`.replace(/\/\//g, '/'),
      },
    ],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: ['@/assets/css/tailwind.css', '@/assets/scss/main.scss', 'animate.css', '@icon-park/vue/styles/index.css'],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    '@/plugins/polyfill',
    '@/plugins/nuxt-client-init',
    '@/plugins/filters',
    '@/plugins/directives',
    '@/plugins/axios',
    '@/plugins/router-after-each',
    '@/plugins/vee-validate',
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: ['~/components', { path: '@/components/base/form', pathPrefix: false, prefix: 'base' }],

  router: {
    base: process.env.ROUTER_ROOT,

    extendRoutes(routes, resolve) {
      routes.push({
        name: 'fallback',
        path: '*',
        component: resolve(__dirname, 'pages/index.vue'),
      });
    },

    middleware: ['auth'],
  },

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    // https://composition-api.nuxtjs.org/
    '@nuxtjs/composition-api/module',
  ],

  eslint: {
    files: ['.', '!node_modules'],
  },

  tailwindcss: {
    exposeConfig: true,
  },

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
  ],

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {
    proxy: true,
  },

  proxy: [
    [
      '^/hn/',
      {
        target: 'http://hn.algolia.com/api/v1',
        pathRewrite: {
          '^/hn': '',
        },
      },
    ],

    [
      (pathname) => pathname.match('^/(security|stb)/'),
      {
        target: 'http://172.21.30.67:20080', // 开发环境
        // target: 'http://172.28.81.20:8082', // 测试环境
      },
    ],
  ],

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    babel: {
      plugins: [
        ['@babel/plugin-proposal-private-methods', { loose: true }],
        [
          'import',
          {
            libraryName: 'vant',
            libraryDirectory: 'es',
            style: (name) => `${name}/style/less`,
          },
          'vant',
        ],
        [
          'import',
          {
            libraryName: '@icon-park/vue',
            libraryDirectory: 'es/icons',
            camel2DashComponentName: false,
          },
        ],
      ],
    },

    extend(config) {
      // alias
      config.resolve.alias.styles = join(config.resolve.alias.assets, 'scss');
    },

    loaders: {
      less: {
        lessOptions: {
          modifyVars: {
            'button-primary-background-color': '#1989fa',
          },
        },
      },
    },

    postcss: {
      plugins: {
        'postcss-px-to-viewport': {
          viewportWidth: 375,
          unitPrecision: 3,
          viewportUnit: 'vw',
          selectorBlackList: [/van-picker/],
          minPixelValue: 1,
          mediaQuery: false,
        },
      },
    },

    transpile: ['vee-validate/dist/rules'],
  },

  env: {},

  publicRuntimeConfig: {
    appName: APP_NAME,
    routerRoot: process.env.ROUTER_ROOT,
    apiRoot: process.env.API_ROOT,
    apiPrefix: process.env.API_PREFIX,
  },

  privateRuntimeConfig: {},

  loading: { color: '#9F7AEA' },

  loadingIndicator: {
    name: 'pulse',
    color: '#4fd1c5',
    background: '#f7fafc',
  },

  server: {
    port: 6300,
    host: '0.0.0.0',
  },

  messages: {
    error_404: '404 NOT FOUND',
    back_to_home: '返回首页',
    nuxtjs: APP_NAME,
  },
};
