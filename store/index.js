import createPersistedState from 'vuex-persistedstate';

// 'auth/updateUser'
export const plugins = [
  createPersistedState({
    key: 'bms_web',
    paths: ['auth.user', 'ui.sidebarCollapsed', 'ui.currentSystem'],
    filter: ({ type }) =>
      ['auth/loginSuccess', 'auth/logoutSuccess', 'ui/toggleSidebar', 'ui/setCurrentSystem'].includes(type),
  }),
];

export const state = () => ({
  cityList: [],
  cityListLoading: false,
  cityListError: null,
});

export const mutations = {
  fetchCityListRequest: (state) => {
    state.cityListLoading = true;
    state.cityList = [];
  },
  fetchCityListSuccess: (state, payload) => {
    state.cityListLoading = false;
    state.cityList = payload;
  },
  fetchCityListError: (state, error) => {
    state.cityListLoading = false;
    state.cityListError = error;
    state.cityList = [];
  },
};

export const actions = {
  async fetchCityList({ commit }) {
    commit('fetchCityListRequest');

    const data = await this.$axios.$get('/stb/lookup/cities', {
      transformData: ({ data }) =>
        (data || []).map((item) => ({
          label: item.value,
          value: item.key,
        })),
    });
    commit('setCityList', data);
  },
};
