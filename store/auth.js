const ADMIN_ID = 'SYS_ADMIN';

export const state = () => ({
  user: {
    userName: 'admin',
    roleList: [{ roleCode: 'SYS_ADMIN' }],
  },
  loginError: null,
  logoutError: null,
  loading: false,
});

export const getters = {
  user: ({ user = {} }) => user,
  userName: (state, { user }) => user.userName,
  token: (state, { user }) => user.token,
  role: (state, { user }) => user.roleList || [],
  isAdmin: (state, { role }) => {
    if (role.length) {
      return role.some((item) => item.roleCode === ADMIN_ID);
    }
    return false;
  },

  isLoggedIn: (state, { userName }) => !!userName,
};

export const mutations = {
  loginRequest(state) {
    state.loading = true;
    state.loginError = null;
  },
  loginSuccess(state, payload) {
    state.loading = false;
    state.user = {
      ...state.user,
      ...payload,
    };
  },
  loginFailure(state, error) {
    state.loading = false;
    state.loginError = error;
  },
  logoutRequest(state) {
    state.loading = true;
    state.logoutError = null;
  },
  logoutSuccess(state) {
    state.loading = false;
    state.hasForcedOut = false;
    state.shouldChangePassword = false;
    state.user = {};
  },
  logoutFailure(state, { message }) {
    state.loading = false;
    state.logoutError = message;
  },
  updateUser(state, payload) {
    if (state.user) {
      state.user = {
        ...state.user,
        ...payload,
      };
    }
  },
  clearError(state) {
    state.loginError = null;
    state.logoutError = null;
  },
};

export const actions = {
  async login({ commit }, payload) {
    commit('loginRequest');

    try {
      const user = await this.$axios.$post('/security/login', payload);

      commit('loginSuccess', user);
    } catch (error) {
      commit('loginFailure', error);
      throw error;
    }
  },

  async logout({ commit }) {
    commit('logoutRequest');

    try {
      await this.$axios.post('/security/logout');
    } catch (error) {
      // logout always succeeds
    }

    commit('logoutSuccess');
  },
};
